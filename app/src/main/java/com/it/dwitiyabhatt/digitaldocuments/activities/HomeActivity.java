package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.adapter.DocsAdapter;
import com.it.dwitiyabhatt.digitaldocuments.adapter.LanguageAdapter;
import com.it.dwitiyabhatt.digitaldocuments.adapter.OptionsAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.fragments.HomeFragment;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.frame_container)
    FrameLayout frameLayout;

    @BindView(R.id.adView)
    AdView adView;


    private Fragment mFragment = null;
    private String loggedInUserId;
    private ArrayList<Document> documentArrayList = new ArrayList<>();
    private FragmentManager fragmentManager;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
         DigitalDocsApp.getmInstance().setActivity(this);
         mFragment = new HomeFragment();
        openFragment(mFragment);

        loggedInUserId = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");

        loadBannerAd();
        getDataFromServer();

    }

    private void loadBannerAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void getDataFromServer() {
        showHideProgress(true,frProgress);

        mDatabase = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(loggedInUserId)
                .child(Constants.DOCUMENTS);



        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                documentArrayList.clear();

                Iterator<DataSnapshot> iter = dataSnapshot.getChildren().iterator();

                if(iter.hasNext()) {

                    while (iter.hasNext()) {
                        Document document = iter.next().getValue(Document.class);
                        documentArrayList.add(document);
                        Toast.makeText(HomeActivity.this, "Coming here", Toast.LENGTH_SHORT).show();
                    }

                }else{

                    Toast.makeText(HomeActivity.this, getString(R.string.msg_add_some_data), Toast.LENGTH_SHORT).show();
                }

                showHideProgress(false,frProgress);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getUpdatedList(){

    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }

    public void openFragment(final Fragment mFragment) {
        final android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, mFragment, mFragment.getClass().getSimpleName());
        transaction.commit();


    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }



    public void setUpToolbar(final String title,boolean showToolbar, final boolean isShowback) {

        toolbar.setVisibility(showToolbar?View.VISIBLE:View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        if(isShowback){
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }else{
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

    }

    public ArrayList<Document> getDocumentArrayList() {
        return documentArrayList;
    }

    public void setDocumentArrayList(ArrayList<Document> documentArrayList) {
        this.documentArrayList = documentArrayList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        if (id == android.R.id.home) {

            fragmentManager = getFragmentManager();

            if (fragmentManager.getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return true;
            }

        }
        return super.onOptionsItemSelected(item);
    }

}
