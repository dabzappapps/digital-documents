package com.it.dwitiyabhatt.digitaldocuments.baseclasses;

import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by user15 on 5/12/17.
 */

public class BaseFragment extends Fragment {




    public void showHideProgress(boolean isShowProgress, FrameLayout progressBar) {
        if (isShowProgress) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setEnabled(false);

        } else {
            progressBar.setVisibility(View.GONE);
            progressBar.setEnabled(true);
        }

    }
    public void showSnackbar(boolean isSnakbar,View parent,String msg){
        try
        {
            if (isSnakbar)
            {
                Snackbar snack = Snackbar.make(parent, msg, Snackbar.LENGTH_LONG);
                //snack.getView().setBackgroundColor(Color.parseColor("#FF7043"));
                View viewNew = snack.getView();
                TextView tv = (TextView) viewNew.findViewById(android.support.design.R.id.snackbar_text);
                tv.setGravity(Gravity.CENTER_HORIZONTAL);
                snack.show();
            } else {
                Toast.makeText(getActivity(),msg, Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e){e.printStackTrace();}

    }
}
