package com.it.dwitiyabhatt.digitaldocuments.util;

public class Constants {
    public static final String ACTION_PLAY="action_play";

    //FirebaseConstants

    public static final String USERS = "users";

    public static final String USER_DETAIL = "user_details";
    public static final String DOCUMENTS= "documents";
    public static final String DOC_PHOTOS= "doc_photos";

    public static final String FILE_TYPE_PDF = "pdf";
    public static final String FILE_TYPE_IMAGE= "image";




    //Storage directory constants
    public static final String MAIN_DIRECTORY = "Digital documents/";
    public static final String RESUME_DIRECTORY = "Resume";

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;

    public static final String STORAGE_PATH_UPLOADS = "uploads/";
    public static final String DATABASE_PATH_UPLOADS = "uploads";
    public static  boolean IS_HOME= false;
    public static final int PICK_IMAGE_REQUEST = 234;
    public static final int UPLOAD_PDF = 235;

    public static final String DOC_TYPE = "doc_type";
    public static final int PDF = 0;
    public static final int PHOTO = 1;
    public static final int AUDIO = 2;
    public static final int VIDEO = 3;
    public static final int LOCK_APP = 4;

}
