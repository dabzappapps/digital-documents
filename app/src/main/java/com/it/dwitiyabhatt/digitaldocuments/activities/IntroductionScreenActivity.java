package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.model.User;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class IntroductionScreenActivity extends BaseActivity {

    @BindView(R.id.rvParent)
    RelativeLayout rvParent;

    @BindView(R.id.sign_in_button)
    SignInButton signInButton;

    @BindView(R.id.ivBg)
    ImageView ivBg;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;


    private DatabaseReference mDatabase;

    private final int RC_SIGN_IN = 234;

    //Tag for the logs optional
    private static final String TAG = "introductionScreen";

    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;



    //And also a Firebase Auth object
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_intro_screen);
        ButterKnife.bind(this);


        signInButton.setSize(SignInButton.SIZE_WIDE);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        //setupCommonData();

        AlphaAnimation animation = new AlphaAnimation(0.0f , 1.0f ) ;
        animation.setFillAfter(true);
        animation.setDuration(1200);

        rvParent.startAnimation(animation);

       /* Glide.with(this).load(R.drawable.intro_bg)
                .apply(bitmapTransform(new BlurTransformation(25,10)))
                .into(ivBg);*/

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }

    private void signIn() {
        if (((ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            showHideProgress(true,frProgress);
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
        else{
            Toast.makeText(this, getString(R.string.msg_no_internet_download_pdf), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                showHideProgress(false,frProgress);
                Toast.makeText(this, "It seems server is busy. Try again in few minutes", Toast.LENGTH_SHORT).show();
                /*Log.w(TAG, "Googlellleee"+ e.getCause() );
                Log.w(TAG, "Googlellleee"+ e.getMessage());
                Log.w(TAG, "Googlellleee"+ e.getLocalizedMessage());
                Log.w(TAG, "Googlellleee code"+e.getStatusCode());*/
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                            DatabaseReference myRef = mDatabase.getDatabase().getReference(Constants.USERS);
                            final User modelUser = new User();
                            modelUser.setuId(user.getUid());
                            modelUser.setEmail(user.getEmail());
                            modelUser.setImgUrl(user.getPhotoUrl().toString());
                            modelUser.setName(user.getDisplayName());
                            myRef.child(modelUser.getuId()).child(Constants.USER_DETAIL).setValue(modelUser)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    DigitalDocsApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_is_logged_in),
                                                    "1");
                                    DigitalDocsApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_id),
                                                    modelUser.getuId());

                                    DigitalDocsApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_email),
                                                    modelUser.getEmail());

                                    DigitalDocsApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_name),
                                                    modelUser.getName());

                                    DigitalDocsApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_img),
                                                    modelUser.getImgUrl());


                                    redirectHomeActivity();
                                }
                            });


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void redirectHomeActivity() {
        showHideProgress(false,frProgress);
        Toast.makeText(IntroductionScreenActivity.this,
                R.string.message_login_success, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(IntroductionScreenActivity.this,HomeActivity2.class));

        finish();
    }


    @OnClick({R.id.sign_in_button})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.sign_in_button:
                /*startActivity(new Intent(IntroductionScreen.this,HomeActivity.class));
                finish();*/
                signIn();
                break;

        }

    }




}
