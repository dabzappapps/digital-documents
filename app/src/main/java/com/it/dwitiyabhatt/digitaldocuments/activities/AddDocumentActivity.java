package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.it.dwitiyabhatt.digitaldocuments.BuildConfig;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.util.AppWebViewClients;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;
import com.it.dwitiyabhatt.digitaldocuments.util.PathUtil;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.it.dwitiyabhatt.digitaldocuments.util.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class AddDocumentActivity extends BaseActivity
        implements OnPageChangeListener,OnLoadCompleteListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.pdfView)
    PDFView pdfView;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.fabShare)
    FloatingActionButton fabShare;

    @BindView(R.id.fabUpload)
    FloatingActionButton fabUpload;


    @BindView(R.id.fabPrint)
    FloatingActionButton fabPrint;

    @BindView(R.id.webView)
    WebView webView;

    private AdView adView;
    private EditText  etTitle;
    private TextView tvDone;

    Integer pageNumber = 0;
    String pdfFileName = "";
    private AlertDialog dialog;
    private boolean isFabOpen = false;
    StorageReference mStorageReference;
    DatabaseReference mDatabaseReference;

    private File file,downloadedFile;
    private String loggedInUserId;
    private Document document;
    private String pushId;
    private boolean isEditMode=false;
    private boolean isDefault=false;
    private Uri pdfUri;
    private AlertDialog alert;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_document);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_VIEW.equals(action) && type != null) {
            pdfUri = intent.getData();
            String filePath = "";
            try {
                filePath= PathUtil.getPath(AddDocumentActivity.this,pdfUri);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            if(filePath !=null){
                file =new File(filePath);
                isDefault = true;
                pdfView.fromUri(pdfUri)
                        .defaultPage(pageNumber)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(new DefaultScrollHandle(AddDocumentActivity.this))
                        .load();
            }else{
                Toast.makeText(this, R.string.error_opening_attached_file, Toast.LENGTH_SHORT).show();
                finish();
            }


        }else{
            startShowingVideoAdd();
        }


        mStorageReference = FirebaseStorage.getInstance().getReference();

        loggedInUserId = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");

        mDatabaseReference = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(loggedInUserId).
                child(Constants.DOCUMENTS);


        if(getIntent() !=null && getIntent().getSerializableExtra("document") !=null){
            isEditMode = true;

            document = (Document) getIntent().getSerializableExtra("document");

           /* if(document.getUpdatedName() != null && !document.getUpdatedName().isEmpty())
                pdfFileName = document.getUpdatedName();
            else
                pdfFileName = document.getUpdatedName();*/

           pdfFileName = document.getName();

            setUpToolbar(getString(R.string.update_document));
            pushId = document.getId();
            if(DigitalDocsApp.getmInstance().getSharedPreferences().
                    getString(getString((R.string.preference_param_has_shown_print_msg)), "").isEmpty()){
                Toast.makeText(this, getString(R.string.msg_share_print_document), Toast.LENGTH_SHORT).show();
                DigitalDocsApp.getmInstance().savePreferenceDataString
                        (getString(R.string.preference_param_has_shown_print_msg),
                                "1");

            }

            checkAndLoadLocalFIle();
            showFABMenu();
        } else {
            pushId = mDatabaseReference.push().getKey();
            setUpToolbar(getString(R.string.add_document));
            document = new Document();
            document.setId(pushId);
            document.setFileType("pdf");
        }

        //setupBannerAd();
    }

    private void checkAndLoadLocalFIle(){
        File mainDir = new File(Environment.getExternalStorageDirectory(),
                Constants.MAIN_DIRECTORY);
        if(mainDir.exists()){
            file  =  new File(mainDir.getAbsolutePath(), pdfFileName+ ".pdf");
            if(file.exists()) displayFromFile(file);
            else downloadPDF();
        }else{
            downloadPDF();
        }

    }

    private void displayFromFile(File file) {

        pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(AddDocumentActivity.this))
               .load();
        isEditMode = true;



    }

    private void setUpToolbar(final String title) {

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
            }
        });

    }

    private void openUploadPDFDialog() {

        if (((ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AddDocumentActivity.this);
            builder.setView(R.layout.layout_doc_title);
            dialog = builder.create();
            // display dialog
            dialog.show();
            dialog.setCancelable(true);
            etTitle = dialog.findViewById(R.id.etTitle);
            tvDone = dialog.findViewById(R.id.tvDone);

            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(etTitle != null && !etTitle.getText().toString().trim().isEmpty()){
                        dialog.dismiss();
                        pdfFileName = etTitle.getText().toString().trim();
                        if(!isDefault){
                            uploadPDF();
                        }else{
                            uploadFile(pdfUri);
                            showFABMenu();
                        }
                    }else{
                        Toast.makeText(AddDocumentActivity.this, R.string.please_enter_document_name, Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            Toast.makeText(AddDocumentActivity.this, getString(R.string._msgno_internet_upload_document), Toast.LENGTH_SHORT).show();

        }




    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //when the user choses the file
        if (requestCode == Constants.UPLOAD_PDF && resultCode == RESULT_OK && data != null && data.getData() != null) {
            //if a file is selected
            if (data.getData() != null) {
                //uploading the file
                if(getMimeType(data.getData()).equalsIgnoreCase("application/pdf")){
                    if (((ConnectivityManager) getSystemService
                            (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                        showHideProgress(true, frProgress);
                        uploadFile(data.getData());
                    }else{
                        Toast.makeText(this, getString(R.string.msg_no_internet_download_pdf), Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(this, R.string.please_upload_pdfs_only, Toast.LENGTH_SHORT).show();
                }

            }else{
                Toast.makeText(AddDocumentActivity.this, R.string.no_file_choosen, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    //this method is uploading the file
    //the code is same as the previous tutorial
    //so we are not explaining it
    private void uploadFile(Uri data) {

            StorageReference sRef = mStorageReference.child(Constants.STORAGE_PATH_UPLOADS +loggedInUserId+
                    "_"+pdfFileName+".pdf");
            sRef.putFile(data)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @SuppressWarnings("VisibleForTests")
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            showHideProgress(false,frProgress);
                            Toast.makeText(AddDocumentActivity.this, R.string.file_uploaded_success, Toast.LENGTH_SHORT).show();
                            // Upload upload = new Upload(editTextFilename.getText().toString(), taskSnapshot.getDownloadUrl().toString());
                            //mDatabaseReference.child(mDatabaseReference.push().getKey()).setValue(upload);

                            downloadPDF();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            showHideProgress(false,frProgress);
                            Toast.makeText(AddDocumentActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @SuppressWarnings("VisibleForTests")
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            //textViewStatus.setText((int) progress + "% Uploading...");
                        }
                    });


    }

    private void downloadPDF(){
        try {

            showHideProgress(true,frProgress);

            if (((ConnectivityManager) getSystemService
                    (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                StorageReference sRef = mStorageReference.child(Constants.STORAGE_PATH_UPLOADS + loggedInUserId
                        + "_" + pdfFileName + ".pdf");
                final File localFile = File.createTempFile("resume", "pdf");

                sRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Local temp file has been created
                        tvNoData.setVisibility(View.GONE);
                        file = localFile;
                        displayFromFile(localFile);
                        saveFileToMyDirectory();
                        showHideProgress(false, frProgress);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(AddDocumentActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                        showHideProgress(false, frProgress);
                        tvNoData.setVisibility(View.VISIBLE);
                    }
                });
            }else{
                Toast.makeText(this, getString(R.string.msg_no_internet_download_pdf), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }


    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    private void showFABMenu(){
        isFabOpen=true;
        fabPrint.animate().translationY(-getResources().getDimension(R.dimen._45sdp));
        fabShare.animate().translationY(-getResources().getDimension(R.dimen._90sdp));
        fabUpload.animate().translationY(-getResources().getDimension(R.dimen._135sdp));
    }

    private void closeFABMenu(){
        isFabOpen=false;
        fabPrint.animate().translationY(0);
        fabShare.animate().translationY(0);
        fabUpload.animate().translationY(0);
    }

    @OnClick({R.id.fabAdd,R.id.fabShare, R.id.fabPrint,R.id.fabUpload})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.fabAdd:
                if(isEditMode || isDefault){
                    if(isFabOpen)closeFABMenu();
                    else showFABMenu();
                }else{
                    openUploadPDFDialog();
                }

                break;
            case R.id.fabShare:
                if(isDefault){
                    checkPermission(AddDocumentActivity.this);
                }else{
                    shareFile();
                }

                break;
            case R.id.fabPrint:
                //downloadPDF();
                printPDF(pdfView);

                break;
            case R.id.fabUpload:

                if(isDefault){
                    if(DigitalDocsApp.getmInstance().getSharedPreferences().
                            getString(getString((R.string.preference_param_user_is_logged_in)), "").isEmpty()){
                        Toast.makeText(AddDocumentActivity.this, getString(R.string.msg_please_log_in), Toast.LENGTH_SHORT).show();
                    }else{
                        openUploadPDFDialog();
                    }

                }else{
                    uploadPDF();
                }

                break;

        }

    }

    private void uploadPDF() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,  getString(R.string.select_file)), Constants.UPLOAD_PDF);
    }

    private void shareFile(){
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            //File fileWithinMyDir =  file;

            if(file !=null && file.exists()) {
                intentShareFile.setType("application/pdf");
                intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Uri uri = FileProvider.getUriForFile(AddDocumentActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",file);
                intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.shating_file));
                intentShareFile.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_file_msg));

                startActivity(Intent.createChooser(intentShareFile, getString(R.string.share_file)));
            }else{
                Toast.makeText(AddDocumentActivity.this, R.string.file_not_found, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveFileToMyDirectory(){
        try {
            File mainDir = new File(Environment.getExternalStorageDirectory(),
                    Constants.MAIN_DIRECTORY );
            if (! mainDir.exists()){
                mainDir.mkdirs();
            }
            downloadedFile  =  new File(mainDir.getAbsolutePath(), pdfFileName+ ".pdf");

            FileInputStream inStream = new FileInputStream(file.getAbsolutePath());
            FileOutputStream outStream = new FileOutputStream(downloadedFile);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            inStream.close();
            outStream.close();


            document.setName(pdfFileName);
            document.setCreatedDate(String.valueOf(System.currentTimeMillis()));
            file = downloadedFile;

            mDatabaseReference.child(pushId).setValue(document).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showHideProgress(false,frProgress);


                }
            });
            showAddWithoutCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void printPDF(View view) {

        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);
        printManager.print(isDefault?"Printed file":pdfFileName, new ViewPrintAdapter(AddDocumentActivity.this,
                view), null);
    }


    public class ViewPrintAdapter extends PrintDocumentAdapter {

        private PrintedPdfDocument mDocument;
        private Context mContext;
        private View mView;

        public ViewPrintAdapter(Context context, View view) {
            mContext = context;
            mView = view;
        }

        @Override
        public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes,
                             CancellationSignal cancellationSignal,
                             LayoutResultCallback callback, Bundle extras) {

            mDocument = new PrintedPdfDocument(mContext, newAttributes);

            if (cancellationSignal.isCanceled()) {
                callback.onLayoutCancelled();
                return;
            }

            PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(1);

            PrintDocumentInfo info = builder.build();
            callback.onLayoutFinished(info, true);
        }



        @Override
        public void onWrite(PageRange[] pages, ParcelFileDescriptor destination,
                            CancellationSignal cancellationSignal,
                            WriteResultCallback callback) {

            // Start the page

            android.graphics.pdf.PdfDocument.Page page = mDocument.startPage(0);
            // Create a bitmap and put it a canvas for the view to draw to. Make it the size of the view
            Bitmap bitmap = Bitmap.createBitmap(mView.getWidth(), mView.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            mView.draw(canvas);
            // create a Rect with the view's dimensions.
            Rect src = new Rect(0, 0, mView.getWidth(), mView.getHeight());
            // get the page canvas and measure it.
            Canvas pageCanvas = page.getCanvas();
            float pageWidth = pageCanvas.getWidth();
            float pageHeight = pageCanvas.getHeight();
            // how can we fit the Rect src onto this page while maintaining aspect ratio?
            float scale = Math.min(pageWidth/src.width(), pageHeight/src.height());
            float left = pageWidth / 2 - src.width() * scale / 2;
            float top = pageHeight / 2 - src.height() * scale / 2;
            float right = pageWidth / 2 + src.width() * scale / 2;
            float bottom = pageHeight / 2 + src.height() * scale / 2;
            RectF dst = new RectF(left, top, right, bottom);

            pageCanvas.drawBitmap(bitmap, src, dst, null);
            mDocument.finishPage(page);

            try {
                mDocument.writeTo(new FileOutputStream(
                        destination.getFileDescriptor()));
            } catch (IOException e) {
                callback.onWriteFailed(e.toString());
                return;
            } finally {
                mDocument.close();
                mDocument = null;
            }
            callback.onWriteFinished(new PageRange[]{new PageRange(0, 0)});
        }
    }

    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(AddDocumentActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showPermissionNeededDialog(context);
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void showPermissionNeededDialog(Context context) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.permission_necessary);
        alertBuilder.setMessage(R.string.external_storage_permission_needed);
        alertBuilder.setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        });


        alert = alertBuilder.create();
        alert.show();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                try {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            shareFile();


                    } else {
                        showPermissionNeededDialog(AddDocumentActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

}
