package com.it.dwitiyabhatt.digitaldocuments.activities;

/**
 * Created by user15 on 20/3/18.
 */

public class DialogItem {

    public final String text;
    public final int icon;
    public DialogItem(String text, Integer icon) {
        this.text = text;
        this.icon = icon;
    }
    @Override
    public String toString() {
        return text;
    }
}
