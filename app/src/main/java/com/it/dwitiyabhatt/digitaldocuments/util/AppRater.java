package com.it.dwitiyabhatt.digitaldocuments.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppRater {
    private final static String APP_TITLE = "Digital documents";// App Name
    private final static String APP_PNAME = "com.it.dwitiyabhatt.digitaldocuments";// Package Name

    private final static int DAYS_UNTIL_PROMPT = 3;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 5;//Min number of launches


    public static void app_launched(Activity activity) {
        SharedPreferences prefs = activity.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) { return ; }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {

                showRateDialog(activity, editor);

        }

        editor.commit();
    }   

    private static void showRateDialog(final Activity mActivity, final SharedPreferences.Editor editor) {
        try{

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);

            alertDialogBuilder.setPositiveButton("SURE", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                    if (editor != null) {
                        editor.putBoolean("dontshowagain", true);
                        editor.commit();
                    }
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.setNegativeButton("LATER", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    if(editor !=null){
                        editor.putLong("launch_count", 1);
                        editor.commit();
                    }

                }
            });

            alertDialogBuilder.setNeutralButton("Complaint", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto","dabzappapps@gmail.com", null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Complaint about Digital Documents");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "My complaints are :");
                    mActivity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
                    if (editor != null) {
                        editor.putBoolean("dontshowagain", true);
                        editor.commit();
                    }
                }
            });

            AlertDialog dialog = alertDialogBuilder.create();
            dialog.setTitle("Rate " + APP_TITLE);
            dialog.setMessage("If you enjoy using Digital Documents, please take a moment to rate it. Thanks for your support!");
            dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    public static void showRateDialog(final Context mContext) {
        try{

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setPositiveButton("Yes, I am ready", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            AlertDialog dialog = alertDialogBuilder.create();
            dialog.setTitle("Rate " + APP_TITLE);
            dialog.setMessage("If you are enjoying using Digital Documents, please take a moment to rate it. Thanks for your support!");

            dialog.show();
            dialog.setCancelable(false);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}