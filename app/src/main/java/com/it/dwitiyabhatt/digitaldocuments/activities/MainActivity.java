package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.adapter.DocsAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.model.Language;
import com.it.dwitiyabhatt.digitaldocuments.util.AdManager;
import com.it.dwitiyabhatt.digitaldocuments.util.AppRater;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;
import com.it.dwitiyabhatt.digitaldocuments.util.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.it.dwitiyabhatt.digitaldocuments.util.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class MainActivity extends BaseActivity
        implements Toolbar.OnMenuItemClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView rvDocs;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.fabDoc)
    FloatingActionButton fabDoc;

    @BindView(R.id.fabCamera)
    FloatingActionButton fabCamera;

    @BindView(R.id.etSearch)
    EditText etSearch;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private ArrayList<Document> documentArrayList = new ArrayList<>();
    private DocsAdapter docsAdapter;
    private DatabaseReference mDatabase;
    private String loggedInUserId;
    private String source ="add";
    private Document document;
    private AlertDialog alert;
    private AlertDialog dialog;
    private EditText  etTitle;
    private TextView tvDone;

    private boolean isFabOpen = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        AppRater.app_launched(MainActivity.this);
        setUpToolbar();
        setupAdapter();

        loggedInUserId = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");

        getDataFromServer();

        startShowingVideoAdd();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    private void getDataFromServer() {
        showHideProgress(true,frProgress);

        mDatabase = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(loggedInUserId)
                .child(Constants.DOCUMENTS);



        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                documentArrayList.clear();
                docsAdapter.notifyDataSetChanged();

                Iterator<DataSnapshot> iter = dataSnapshot.getChildren().iterator();

                if(iter.hasNext()) {
                    tvNoData.setVisibility(View.GONE);
                    while (iter.hasNext()) {
                        Document company = iter.next().getValue(Document.class);
                        documentArrayList.add(company);
                    }
                    docsAdapter.notifyDataSetChanged();
                }else{
                    tvNoData.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, getString(R.string.msg_add_some_data), Toast.LENGTH_SHORT).show();
                }

                showHideProgress(false,frProgress);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void setUpToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_doc_list);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        toolbar.setOnMenuItemClickListener(this);

    }

    @OnClick({R.id.fabAdd,R.id.fabDoc,R.id.fabCamera})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.fabAdd:
                if(isFabOpen)closeFABMenu();
                else showFABMenu();

                break;
            case R.id.fabDoc:
                source = "add";
                if(checkPermission(MainActivity.this)){
                    startActivity(new Intent(MainActivity.this,AddDocumentActivity.class));

                }

                break;

            case R.id.fabCamera:
                if(checkPermission(MainActivity.this)) {
                    startActivity(new Intent(MainActivity.this, AddImageActivity.class));
                }
                break;

        }

    }

    private void setupAdapter() {
        rvDocs.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        docsAdapter = new DocsAdapter(MainActivity.this, documentArrayList);
        rvDocs.setAdapter(docsAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                docsAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                try {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if(source.equals("update")){
                            openUpdateDocActivity();
                        }else{
                            startActivity(new Intent(MainActivity.this,AddDocumentActivity.class));
                        }

                    } else {
                        showPermissionNeededDialog(MainActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showPermissionNeededDialog(context);
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void showPermissionNeededDialog(Context context) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.permission_necessary);
        alertBuilder.setMessage(R.string.external_storage_permission_needed);
        alertBuilder.setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                }
        });


            alert = alertBuilder.create();
            alert.show();


    }

    private void deleteConfirmDialog(Context context, final Document document) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.delete_title);
        alertBuilder.setMessage(R.string.remove_doc_confirmation);
        alertBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {

                mDatabase.child(document.getId()).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        showHideProgress(false,frProgress);
                        alert.dismiss();
                        Toast.makeText(MainActivity.this, R.string.doc_removed_success, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        alertBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
           alert.dismiss();
            }
        });


        alert = alertBuilder.create();
        alert.show();


    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
        @Override
        public void onItemClick(View view, final int position)
        {
            document = docsAdapter.getDocListFiltered().get(position);
            switch (view.getId()){
                case R.id.tvRename:
                    openRenameDialog(document);
                    break;
                case R.id.tvDelete:
                    if (((ConnectivityManager) getSystemService
                            (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {

                        deleteConfirmDialog(MainActivity.this,document);
                    }else {
                        Toast.makeText(MainActivity.this, getString(R.string._msgno_internet_upload_document), Toast.LENGTH_SHORT).show();

                    }
                    break;

                case R.id.tvUpdate:
                    source = "update";
                    if(checkPermission(MainActivity.this)){
                        if(checkPermission(MainActivity.this)){
                            openUpdateDocActivity();
                        }

                    }

                    break;
                    default:
                        source = "update";

                        document = docsAdapter.getDocListFiltered().get(position);

                        if(checkPermission(MainActivity.this)){
                            if(checkPermission(MainActivity.this)){
                                openUpdateDocActivity();
                            }

                        }

            }
                    }
    };

    private void openUpdateDocActivity() {

        Intent intent;

        if(document.getFileType().equals("image")){
            intent = new Intent(MainActivity.this,AddImageActivity.class);
        }else{
            intent = new Intent(MainActivity.this,AddDocumentActivity.class);
        }

        /*intent.putExtra("id",document.getId());
        intent.putExtra("name",document.getName());*/
        intent.putExtra("document",document);
        startActivity(intent);
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()){

            case R.id.action_lock:
                try{
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;


            case R.id.action_language:
                try{
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                    Intent intent = new Intent(MainActivity.this,ChangeLanguageActivity.class);
                    intent.putExtra("source","main");
                    startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case R.id.action_share_app:
                try{
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "Hey check out my app at: https://play.google.com/store/apps/details?id=com.it.dwitiyabhatt.digitaldocuments");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case R.id.action_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setMessage(R.string.logout_confirm)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                DigitalDocsApp.getmInstance().clearePreferenceData();
                                Utils.restartApp(MainActivity.this);
                            }



                        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
                break;
        }

        return false;
    }




    private void openRenameDialog(final Document document) {

        if (((ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setView(R.layout.layout_doc_title);
            dialog = builder.create();
            // display dialog
            dialog.show();
            dialog.setCancelable(true);
            etTitle = dialog.findViewById(R.id.etTitle);
            tvDone = dialog.findViewById(R.id.tvDone);

            if(!document.getUpdatedName().isEmpty()) etTitle.setText(document.getUpdatedName());
            else etTitle.setText(document.getName());
            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(etTitle != null && !etTitle.getText().toString().trim().isEmpty()){
                        dialog.dismiss();
                        showHideProgress(true,frProgress);
                        document.setUpdatedName(etTitle.getText().toString().trim());
                        mDatabase.child(document.getId())
                                .setValue(document)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                showHideProgress(false,frProgress);
                                Toast.makeText(MainActivity.this, R.string.doc_renamed_successfully, Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showHideProgress(false,frProgress);
                                Toast.makeText(MainActivity.this, R.string.doc_rename_error, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }else{
                        Toast.makeText(MainActivity.this, R.string.please_enter_document_name, Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            Toast.makeText(MainActivity.this, getString(R.string._msgno_internet_upload_document), Toast.LENGTH_SHORT).show();

        }




    }

    private void showFABMenu(){
        isFabOpen=true;
        fabDoc.animate().translationY(-getResources().getDimension(R.dimen._45sdp));
        fabCamera.animate().translationY(-getResources().getDimension(R.dimen._90sdp));

    }

    private void closeFABMenu(){
        isFabOpen=false;
        fabDoc.animate().translationY(0);
        fabCamera.animate().translationY(0);

    }



}
