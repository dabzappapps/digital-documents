package com.it.dwitiyabhatt.digitaldocuments.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.activities.HomeActivity2;
import com.it.dwitiyabhatt.digitaldocuments.adapter.LanguageAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Language;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;
import com.it.dwitiyabhatt.digitaldocuments.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeLanguageFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView rvLanguage;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ArrayList<Language> languageArrayList = new ArrayList<>();
    private LanguageAdapter languageAdapter;
    private Activity mActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.activity_languages, container, false);
        ButterKnife.bind(this, layout);
        mActivity = DigitalDocsApp.getmInstance().getActivity();
        initToolbar();

        toolbar.setVisibility(View.GONE);
        languageArrayList.add(new Language("English","en"));
        languageArrayList.add(new Language("French","fr"));
        languageArrayList.add(new Language("German","ge"));
        languageArrayList.add(new Language("Spanish","es"));
        languageArrayList.add(new Language("Arabic","ar"));
        languageArrayList.add(new Language("japanese","jp"));
        languageArrayList.add(new Language("Bangla","bn"));
        languageArrayList.add(new Language("Punjabi","pj"));
        languageArrayList.add(new Language("Tamil","ta"));
        languageArrayList.add(new Language("Gujarati","gu"));
        setupAdapter();
        return layout;
    }

    public void initToolbar() {

        mActivity = DigitalDocsApp.getmInstance().getActivity();
        ((HomeActivity2) getActivity()).
                setUpToolbar(getString(R.string.title_change_language), true,false);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden)initToolbar();
    }

    private void setupAdapter() {
        rvLanguage.setLayoutManager(new LinearLayoutManager(mActivity));
        languageAdapter = new LanguageAdapter(mActivity, languageArrayList);
        rvLanguage.setAdapter(languageAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));
    }



    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position)
                {
                    Language language = languageArrayList.get(position);
                    switch (view.getId()){
                        case R.id.linLanguage:
                            languageAdapter.setLastSelectedPosition(position);
                            //languageAdapter.notifyDataSetChanged();
                            DigitalDocsApp.getmInstance().savePreferenceDataString
                                    (getString(R.string.preference_param_language),
                                            language.getCode());
                            Utils.restartApp(mActivity);

                            break;

                    }
                }
            };


}
