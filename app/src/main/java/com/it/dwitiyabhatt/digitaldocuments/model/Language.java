package com.it.dwitiyabhatt.digitaldocuments.model;

public class Language {

    private String language;
    private String code;



    public Language(String language, String code) {
        this.language = language;
        this.code = code;
        }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }


}
