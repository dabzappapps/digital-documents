package com.it.dwitiyabhatt.digitaldocuments.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.model.Language;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;

public class LanguageAdapter extends
        BaseRecyclerAdapter<LanguageAdapter.DataViewHolder, Language>{


    private ArrayList<Language> languagesList;
    private Context context;
    private boolean singleSelection=true;
    private int lastSelectedPosition = 0;

    public LanguageAdapter(Context context, ArrayList<Language> languagesList) {
        super(languagesList);
        this.languagesList = languagesList;
        this.context = context;
    }

    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_language,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        Language language = languagesList.get(position);

        holder.tvLanguage.setText(language.getLanguage());

        String selectedLanguage = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(context.getString((R.string.preference_param_language)), "");

        if(language.getCode().equalsIgnoreCase(selectedLanguage)){
            holder.tvLanguage.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
            holder.tvLanguage.setCompoundDrawablesWithIntrinsicBounds(context.getDrawable(R.drawable.radio_filled),null,null,null);
        }else{
            holder.tvLanguage.setTextColor(ContextCompat.getColor(context,R.color.color_black));
            holder.tvLanguage.setCompoundDrawablesWithIntrinsicBounds(context.getDrawable(R.drawable.radio_empty),null,null,null);
        }
    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.linLanguage)
        LinearLayout linLanguage;

        @BindView(R.id.tvLanguage)
        TextView tvLanguage;



        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);
            clickableViews(linLanguage);
        }
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }


}

