package com.it.dwitiyabhatt.digitaldocuments.baseclasses;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.activities.MainActivity;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.util.AdManager;


/**
 * Created by user15 on 21/11/17.
 */

public class BaseActivity extends AppCompatActivity implements AdManager.AdManagerEvents{



    private ProgressBar progressBar;
    private  RelativeLayout layout;
    private android.support.v7.app.AlertDialog dialog;
    private int count = 1 ;
    private AdManager adManager;
    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //adManager.showAdd();
        //if(count>4)
        adManager = new AdManager(BaseActivity.this,this);



    }

    public void startShowingVideoAdd(){
        count = Integer.parseInt(DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_add_count)), "1"))+ 1;

        int i = count;
        DigitalDocsApp.getmInstance().savePreferenceDataString
                (getString(R.string.preference_param_add_count),
                        String.valueOf(i));




        if(count>4)
        adManager.showVideoAdd();
    }

    public void showAddWithoutCount(){
        adManager.showVideoAdd();
    }

    public void startLoadingVideoAdd(){
        count = Integer.parseInt(DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_add_count)), "1"))+ 1;

        int i = count;
        DigitalDocsApp.getmInstance().savePreferenceDataString
                (getString(R.string.preference_param_add_count), String.valueOf(i));

        adManager.loadRewardedVideoAd();
    }


    public void setProgressBar(Activity activity){
        if(activity !=null){
            progressBar = new ProgressBar(activity,null,android.R.attr.progressBarStyleLarge);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);

            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            //relMain.addView(progressBar,params);
            progressBar.setVisibility(View.GONE);
        }

    }



     public void showSnackbar(boolean isSnakbar,View parent,String msg){
         try
         {
             if (isSnakbar)
             {
                 Snackbar snack = Snackbar.make(parent, msg, Snackbar.LENGTH_LONG);
                 //snack.getView().setBackgroundColor(Color.parseColor("#FF7043"));
                 View viewNew = snack.getView();
                 TextView tv = (TextView) viewNew.findViewById(android.support.design.R.id.snackbar_text);
                 tv.setGravity(Gravity.CENTER_HORIZONTAL);
                 snack.show();
             } else {
                 Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_LONG).show();
             }
         }
         catch (Exception e){e.printStackTrace();}

     }

     public void showHideProgress(boolean isShowProgress, FrameLayout progressBar) {
         try {
             if (isShowProgress) {
                 progressBar.setVisibility(View.VISIBLE);
                 progressBar.setEnabled(false);

             } else {
                 progressBar.setVisibility(View.GONE);
                 progressBar.setEnabled(true);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

     }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();
        return super.dispatchTouchEvent(ev);
    }

  /*  public void showHideProgress(boolean isShowProgress) {
        if (isShowProgress) {
            LayoutInflater inflater = getLayoutInflater();
            progressBar = (ProgressBar ) inflater.inflate(R.layout.progress_bar, null);
            progressBar.setVisibility(View.VISIBLE);

        } else {
            progressBar.setVisibility(View.GONE);
        }

    }*/
  @Override
  public void onAddLoaded() {

  }

    @Override
    public void onAddShown() {

    }

    @Override
    public void onAddClosed() {
        DigitalDocsApp.getmInstance().savePreferenceDataString
                (getString(R.string.preference_param_add_count),
                        "1");

        count=1;


    }
}
