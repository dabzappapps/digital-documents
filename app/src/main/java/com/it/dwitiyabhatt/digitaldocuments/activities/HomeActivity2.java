package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.fragments.ChangeLanguageFragment;
import com.it.dwitiyabhatt.digitaldocuments.fragments.HomeFragment;
import com.it.dwitiyabhatt.digitaldocuments.fragments.PDFListFragment;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;
import com.it.dwitiyabhatt.digitaldocuments.util.Utils;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity2 extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.adView)
    AdView adView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private View headerView;
    private Fragment mFragment = null;
    private String loggedInUserId;
    private ArrayList<Document> documentArrayList = new ArrayList<>();
    private FragmentManager fragmentManager;
    private DatabaseReference mDatabase;
    private String imgUrl,name,email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_2);
        ButterKnife.bind(this);
        DigitalDocsApp.getmInstance().setActivity(this);
        getNeededParams();
        setupDrawer();
        //getDataFromServer();
        loadBannerAd();
        openDefaultFragment();
        startShowingVideoAdd();
    }

    private void openDefaultFragment() {
        mFragment = new PDFListFragment();
        openFragment(mFragment);
    }

    private void getNeededParams() {
        loggedInUserId = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");
        imgUrl = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_img)), "");
        name = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_name)), "");
        email = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_email)), "");
    }

    private void setupDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        headerView = navigationView.inflateHeaderView(R.layout.nav_header_home);
        final ImageView imageView = headerView.findViewById(R.id.ivProfile);
        TextView tvName = headerView.findViewById(R.id.tvName);
        TextView tvEmail = headerView.findViewById(R.id.tvEmail);

        tvEmail.setText(email);
        tvName.setText(name);

        Glide.with(HomeActivity2.this).load(imgUrl).asBitmap().
                placeholder(R.drawable.thumb_personel).centerCrop().into
                (new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                .create(HomeActivity2.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    private void loadBannerAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void getDataFromServer() {


        mDatabase = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(loggedInUserId)
                .child(Constants.DOCUMENTS);



        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                documentArrayList.clear();

                Iterator<DataSnapshot> iter = dataSnapshot.getChildren().iterator();

                if(iter.hasNext()) {

                    while (iter.hasNext()) {
                        Document document = iter.next().getValue(Document.class);
                        documentArrayList.add(document);
                        Toast.makeText(HomeActivity2.this, "Coming here", Toast.LENGTH_SHORT).show();
                    }

                }else{

                    Toast.makeText(HomeActivity2.this, getString(R.string.msg_add_some_data), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getUpdatedList(){

    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }

    public void openFragment(final Fragment mFragment) {
        final android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, mFragment, mFragment.getClass().getSimpleName());
        transaction.commit();


    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }



    public void setUpToolbar(final String title,boolean showToolbar, final boolean isShowback) {

        toolbar.setVisibility(showToolbar?View.VISIBLE:View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(isShowback ? R.drawable.ic_back : R.drawable.ic_menu);
        toolbar.setTitle(title);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public ArrayList<Document> getDocumentArrayList() {
        return documentArrayList;
    }

    public void setDocumentArrayList(ArrayList<Document> documentArrayList) {
        this.documentArrayList = documentArrayList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();
        if (id == android.R.id.home) {

            fragmentManager = getFragmentManager();
            if (fragmentManager.getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return true;
            }
            else {
                drawerLayout.openDrawer(GravityCompat.START);
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Bundle b = new Bundle();
        switch (id){
            case R.id.nav_pdf:
                b.putInt(Constants.DOC_TYPE,Constants.PDF);
                mFragment = new PDFListFragment();
                mFragment.setArguments(b);
                openFragment(mFragment);
                break;
            case R.id.nav_img:
                b.putInt(Constants.DOC_TYPE,Constants.PHOTO);
                mFragment = new PDFListFragment();
                mFragment.setArguments(b);
                openFragment(mFragment);
                break;
            case R.id.nav_lang:
                try{
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                   /* Intent intent = new Intent(HomeActivity2.this,ChangeLanguageActivity.class);
                    intent.putExtra("source","main");
                    startActivity(intent);*/
                   mFragment = new ChangeLanguageFragment();
                   openFragment(mFragment);

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case R.id.nav_logout:
                showLogoutDialog();
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showLogoutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.logout_confirm)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        DigitalDocsApp.getmInstance().clearePreferenceData();
                        Utils.restartApp(HomeActivity2.this);
                    }



                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }



}
