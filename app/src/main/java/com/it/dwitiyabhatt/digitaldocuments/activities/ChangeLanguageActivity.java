package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.it.dwitiyabhatt.digitaldocuments.R;

import com.it.dwitiyabhatt.digitaldocuments.adapter.LanguageAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Language;
import com.it.dwitiyabhatt.digitaldocuments.util.Utils;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeLanguageActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView rvLanguage;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ArrayList<Language> languageArrayList = new ArrayList<>();
    private LanguageAdapter languageAdapter;
    private String source;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languages);
        ButterKnife.bind(this);

        source = getIntent().getStringExtra("source");

        if(source.equalsIgnoreCase("splash")){
            tvTitle.setVisibility(View.VISIBLE);
        }else{
            tvTitle.setVisibility(View.GONE);
        }

        setUpToolbar();

        languageArrayList.add(new Language("English","en"));
        languageArrayList.add(new Language("French","fr"));
        languageArrayList.add(new Language("German","ge"));
        languageArrayList.add(new Language("Spanish","es"));
        languageArrayList.add(new Language("Arabic","ar"));
        languageArrayList.add(new Language("japanese","jp"));
        languageArrayList.add(new Language("Bangla","bn"));
        languageArrayList.add(new Language("Punjabi","pj"));
        languageArrayList.add(new Language("Tamil","ta"));
        languageArrayList.add(new Language("Gujarati","gu"));



        setupAdapter();

    }
    private void setupAdapter() {
        rvLanguage.setLayoutManager(new LinearLayoutManager(ChangeLanguageActivity.this));
        languageAdapter = new LanguageAdapter(ChangeLanguageActivity.this, languageArrayList);
        rvLanguage.setAdapter(languageAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));

    }


    private void setUpToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
            if(source.equalsIgnoreCase("splash")){
                getSupportActionBar().setTitle(R.string.title_select_language);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            }else{
                getSupportActionBar().setTitle(R.string.title_change_language);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
            }
        });
    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position)
                {
                    Language language = languageArrayList.get(position);
                    switch (view.getId()){
                        case R.id.linLanguage:
                            languageAdapter.setLastSelectedPosition(position);
                            //languageAdapter.notifyDataSetChanged();
                            DigitalDocsApp.getmInstance().savePreferenceDataString
                                    (getString(R.string.preference_param_language),
                                            language.getCode());
                            //Utils.setLanguage(ChangeLanguageActivity.this,language.getCode());
                            /*if(source.equalsIgnoreCase("splash")){
                                Intent mLoginIntent = new Intent(ChangeLanguageActivity.this, IntroTabsActivity.class);
                                startActivity(mLoginIntent);
                                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                                finish();
                            }else{
                                Utils.restartApp(ChangeLanguageActivity.this);
                            }*/
                            Utils.restartApp(ChangeLanguageActivity.this);

                            break;

                    }
                }
            };


}
