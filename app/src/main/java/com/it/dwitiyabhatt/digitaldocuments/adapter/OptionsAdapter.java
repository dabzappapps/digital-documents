package com.it.dwitiyabhatt.digitaldocuments.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Language;
import com.it.dwitiyabhatt.digitaldocuments.model.Option;

import java.util.ArrayList;

import butterknife.BindView;

public class OptionsAdapter extends
        BaseRecyclerAdapter<OptionsAdapter.DataViewHolder, Option> {


    private ArrayList<Option> optionArrayList;
    private Context context;
    private boolean singleSelection=true;
    private int lastSelectedPosition = 0;

    public OptionsAdapter(Context context, ArrayList<Option> optionArrayList) {
        super(optionArrayList);
        this.optionArrayList = optionArrayList;
        this.context = context;
    }

    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_option,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        Option option = optionArrayList.get(position);

        holder.tvOptionName.setText(option.getTitle());
        Glide.with(context).load(option.getIcon()).into(holder.ivOptionImage);
    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.ivOptionImage)
        ImageView ivOptionImage;

        @BindView(R.id.tvOptionName)
        TextView tvOptionName;


        @BindView(R.id.cardView)
        CardView cardView;



        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);
            clickableViews(cardView);
        }
    }



}

