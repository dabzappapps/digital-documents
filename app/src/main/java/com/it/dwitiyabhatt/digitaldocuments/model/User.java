package com.it.dwitiyabhatt.digitaldocuments.model;

import android.support.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@Keep
@IgnoreExtraProperties
public class User implements Serializable{

    public String uId,name,email,imgUrl,pinNo;
    public boolean isPinActive=false;

    public User() {
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPinNo() {
        return pinNo;
    }

    public void setPinNo(String pinNo) {
        this.pinNo = pinNo;
    }

    public boolean isPinActive() {
        return isPinActive;
    }

    public void setPinActive(boolean pinActive) {
        isPinActive = pinActive;
    }
}
