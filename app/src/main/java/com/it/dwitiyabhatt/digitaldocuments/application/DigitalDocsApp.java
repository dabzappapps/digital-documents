package com.it.dwitiyabhatt.digitaldocuments.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.FirebaseDatabase;
import com.it.dwitiyabhatt.digitaldocuments.R;

import io.fabric.sdk.android.Fabric;

public class DigitalDocsApp extends Application{

    private static DigitalDocsApp mInstance;
    private SharedPreferences sharedPreferences;
    private Activity activity;


    public static DigitalDocsApp getmInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mInstance = this;
        MobileAds.initialize(this, "ca-app-pub-6140807599924450~5615607509");
        sharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public Activity getActivity() { return activity; }
    public void setActivity(Activity activity) { this.activity = activity; }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mInstance != null) {
            mInstance = null;
        }

    }
    public void savePreferenceDataString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }


    public void clearePreferenceData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
