package com.it.dwitiyabhatt.digitaldocuments.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AdManager {

    private Context mContext;
    private InterstitialAd mInterstitialAd;
    private String interStatialAddId = "ca-app-pub-6140807599924450/2929341207";
    private String videoAddId ="ca-app-pub-6140807599924450/6971586046";
    private String demoInterstetialAddId = "ca-app-pub-3940256099942544/1033173712";
    private String demoVideoAddId = "ca-app-pub-3940256099942544/5224354917";
    private AdManagerEvents adManagerEvents;
    private RewardedVideoAd mRewardedVideoAd;

    public AdManager(Context mContext,AdManagerEvents adManagerEvents) {
        this.mContext = mContext;
        this.adManagerEvents = adManagerEvents;
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId(demoInterstetialAddId);
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(mContext);

        }

        public void showVideoAdd(){
        if(mRewardedVideoAd.isLoaded()){
            adManagerEvents.onAddLoaded();
            mRewardedVideoAd.show();
        }
    }

    public void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd(videoAddId,
                new AdRequest.Builder().build());

        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                Log.d("dd_video_add", "Add loaded");
            }

            @Override
            public void onRewardedVideoAdOpened() {

            }

            @Override
            public void onRewardedVideoStarted() {
                Log.d("dd_video_add", "Add started");
            }

            @Override
            public void onRewardedVideoAdClosed() {
                adManagerEvents.onAddClosed();
                Log.d("dd_video_add", "closed");

            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {

            }

            @Override
            public void onRewardedVideoCompleted() {

            }
        });
    }

    public void loadInterstatialAdd(){

        final AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                adManagerEvents.onAddLoaded();
                showAdd();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            adManagerEvents.onAddClosed();
            }
        });
    }

    public void showAdd(){
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();


        }
    }

    public interface AdManagerEvents{
        public void onAddLoaded();
        public void onAddShown();
        public void onAddClosed();
    }

}
