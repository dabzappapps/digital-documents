package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.it.dwitiyabhatt.digitaldocuments.R;

public class IntroTabsActivity extends AppIntro2 {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFadeAnimation();
        addSlide(AppIntroFragment.newInstance(getString(R.string.save),
                getString(R.string.save_screen_msg),
                R.drawable.cloud,
                ContextCompat.getColor(this,R.color.color_black)));

        addSlide(AppIntroFragment.newInstance(getString(R.string.print), getString(R.string.print_screen_msg), R.drawable.printer,
                ContextCompat.getColor(this,R.color.color_black)));

        addSlide(AppIntroFragment.newInstance(getString(R.string.share_title),
                getString(R.string.share_screen_msg), R.drawable.collaboration,
                ContextCompat.getColor(this,R.color.color_black)));

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        redirectToTintro();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        redirectToTintro();
    }

    private void redirectToTintro() {
        Intent mLoginIntent = new Intent(IntroTabsActivity.this, IntroductionScreenActivity.class);
        startActivity(mLoginIntent);
        overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
        finish();
    }

}
