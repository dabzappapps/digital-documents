package com.it.dwitiyabhatt.digitaldocuments.model;

import android.support.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@Keep
@IgnoreExtraProperties
public class Document implements Serializable{

    public String id,name,cloudPath,filePath,createdDate,updatedName="",fileType="";

    public Document() {
    }

    public Document(String id, String name, String cloudPath, String filePath, String createdDate) {
        this.id = id;
        this.name = name;
        this.cloudPath = cloudPath;
        this.filePath = filePath;
        this.createdDate = createdDate;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCloudPath() {
        return cloudPath;
    }

    public void setCloudPath(String cloudPath) {
        this.cloudPath = cloudPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
