package com.it.dwitiyabhatt.digitaldocuments.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.activities.AddDocumentActivity;
import com.it.dwitiyabhatt.digitaldocuments.activities.HomeActivity;
import com.it.dwitiyabhatt.digitaldocuments.activities.HomeActivity2;
import com.it.dwitiyabhatt.digitaldocuments.activities.MainActivity;
import com.it.dwitiyabhatt.digitaldocuments.adapter.OptionsAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Option;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;
import com.it.dwitiyabhatt.digitaldocuments.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.it.dwitiyabhatt.digitaldocuments.util.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.rvOptions)
    RecyclerView rvOptions;

    private ArrayList<Option> optionArrayList;
    private OptionsAdapter optionsAdapter;
    private Activity mActivity;
    private AlertDialog alert;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, layout);
        optionArrayList = new ArrayList<>();
        optionArrayList.add(new Option(1,"Your Documents",R.drawable.pdf));
        optionArrayList.add(new Option(2,"Your Photos",R.drawable.camera_ic));
        optionArrayList.add(new Option(3,"Your Audios",R.drawable.audio));
        optionArrayList.add(new Option(4,"Your Videos",R.drawable.video));
        optionArrayList.add(new Option(5,"Lock App",R.drawable.pdf));
        optionArrayList.add(new Option(6,"Change Language",R.drawable.translate));
        optionArrayList.add(new Option(7,"Share App",R.drawable.share_icon));
        optionArrayList.add(new Option(8,"Sign out",R.drawable.sign_out));

         setupAdapter();
         initToolbar();
         return layout;
    }

    public void initToolbar() {

        mActivity = DigitalDocsApp.getmInstance().getActivity();

        ((HomeActivity2) getActivity()).
                setUpToolbar(getString(R.string.home), true,false);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden) initToolbar();
    }

    private void setupAdapter() {
        rvOptions.setLayoutManager(new GridLayoutManager(getActivity(),2));
        optionsAdapter = new OptionsAdapter(getActivity(), optionArrayList);
        rvOptions.setAdapter(optionsAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));

    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position)
                {
                    Bundle b = new Bundle();
                    switch (position){
                        case Constants.PDF:
                            if(checkPermission(mActivity)){
                               b.putInt(Constants.DOC_TYPE,Constants.PDF);
                               PDFListFragment pdfListFragment = new PDFListFragment();
                               pdfListFragment.setArguments(b);
                                Utils.addNextFragment(mActivity,pdfListFragment,
                                        HomeFragment.this,false);

                                }

                            break;
                        case Constants.PHOTO:
                            if(checkPermission(mActivity)){
                                b.putInt(Constants.DOC_TYPE,Constants.PHOTO);
                                PDFListFragment pdfListFragment = new PDFListFragment();
                                pdfListFragment.setArguments(b);
                                Utils.addNextFragment(mActivity,pdfListFragment,
                                        HomeFragment.this,false);


                            }

                            break;
                        case Constants.VIDEO:
                            if(checkPermission(mActivity)){

                                VideoListFragment videoListFragment = new VideoListFragment();
                                Utils.addNextFragment(mActivity,videoListFragment,
                                        HomeFragment.this,false);


                            }

                            break;


                    }


                }
            };

    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showPermissionNeededDialog(context);
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void showPermissionNeededDialog(Context context) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.permission_necessary);
        alertBuilder.setMessage(R.string.external_storage_permission_needed);
        alertBuilder.setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + mActivity.getPackageName()));
                startActivity(intent);
            }
        });


        alert = alertBuilder.create();
        alert.show();


    }

}
