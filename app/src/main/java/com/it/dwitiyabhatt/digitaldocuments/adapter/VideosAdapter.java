package com.it.dwitiyabhatt.digitaldocuments.adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;

/**
 * Created by user15 on 9/11/17.
 */

public class VideosAdapter
        extends BaseRecyclerAdapter<VideosAdapter.DataViewHolder, Document> implements Filterable {


    private ArrayList<Document> docList;
    private List<Document> docListFiltered;
    private Context context;
    private boolean singleSelection=true;
    private int lastSelectedPosition = 0;

    public VideosAdapter(Context context, ArrayList<Document> docList) {
        super(docList);
        this.docList = docList;
        this.context = context;
        this.docListFiltered = docList;

    }

    public VideosAdapter(ArrayList<Document> categoryArrayList, Context context, boolean singleSelection) {
        super(categoryArrayList);
        this.docList = categoryArrayList;
        this.context = context;
        this.singleSelection = singleSelection;
    }

    public ArrayList<Document> getNavItemArrayList() {
        return docList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_video,
                parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        Document document = docListFiltered.get(position);
        if(!document.getUpdatedName().isEmpty()) holder.tvTitle.setText(document.getUpdatedName());
        else holder.tvTitle.setText(document.getName());
        holder.tvDate.setText(getDate(Long.parseLong(document.getCreatedDate())));
        holder.ivThumbNail.setImageDrawable(context.getResources().getDrawable(R.drawable.image_icon));

    }

    public List<Document> getDocListFiltered() {
        return docListFiltered;
    }

    @Override
    public int getItemCount() {
        return docListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    docListFiltered = docList;
                } else {
                    List<Document> filteredList = new ArrayList<>();
                    for (Document row : docList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if(row.getUpdatedName()!= null && !row.getUpdatedName().isEmpty()){
                            if (row.getUpdatedName().toLowerCase().
                                    contains(charString.toLowerCase()) || row.getUpdatedName().contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }else{
                            if (row.getName().toLowerCase().
                                    contains(charString.toLowerCase()) || row.getName().contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                    }

                    docListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = docListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                docListFiltered = (ArrayList<Document>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.tvRename)
        TextView tvRename;

        @BindView(R.id.tvUpdate)
        TextView tvUpdate;

        @BindView(R.id.tvDelete)
        TextView tvDelete;

        @BindView(R.id.ivThumbNail)
        ImageView ivThumbNail;

        @BindView(R.id.tvDate)
        TextView tvDate;

        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);
            clickableViews(tvRename);
            clickableViews(tvUpdate);
            clickableViews(tvDelete);

        }
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd MMM yyyy hh:mm aa", cal).toString();
        return date;
    }
}

