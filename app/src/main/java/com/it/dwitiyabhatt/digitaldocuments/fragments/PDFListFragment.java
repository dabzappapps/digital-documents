package com.it.dwitiyabhatt.digitaldocuments.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.activities.AddDocumentActivity;
import com.it.dwitiyabhatt.digitaldocuments.activities.AddImageActivity;
import com.it.dwitiyabhatt.digitaldocuments.activities.HomeActivity2;
import com.it.dwitiyabhatt.digitaldocuments.activities.MainActivity;
import com.it.dwitiyabhatt.digitaldocuments.adapter.DocsAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.it.dwitiyabhatt.digitaldocuments.util.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class PDFListFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView rvDocs;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.etSearch)
    EditText etSearch;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    private AlertDialog alert;
    private ArrayList<Document> pdfList;
    private DocsAdapter docsAdapter;
    private AlertDialog dialog;
    private EditText  etTitle;
    private TextView tvDone;
    private DatabaseReference mDatabase;
    private Activity mActivity;
    private Document document;
    private String source ="add";
    private String loggedInUserId;
    private int docType = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_common_list_view, container, false);
        ButterKnife.bind(this, layout);

        if(getArguments()!=null){
            docType = getArguments().getInt(Constants.DOC_TYPE);
        }

        loggedInUserId = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");

        initToolbar();
        setupList();
        getDataFromServer();
        return layout;
    }

    private void setupList() {

        pdfList = new ArrayList<>();
        rvDocs.setLayoutManager(new LinearLayoutManager(mActivity));
        docsAdapter = new DocsAdapter(mActivity, pdfList);
        rvDocs.setAdapter(docsAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                docsAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void getDataFromServer() {


        mDatabase = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(loggedInUserId)
                .child(Constants.DOCUMENTS);



        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pdfList.clear();
                Iterator<DataSnapshot> iter = dataSnapshot.getChildren().iterator();
                ArrayList<Document> documentArrayList = new ArrayList<>();
                if(iter.hasNext()) {
                    while (iter.hasNext()) {
                        Document document = iter.next().getValue(Document.class);
                        documentArrayList.add(document);
                    }
                    for(Document document : documentArrayList){
                        if(docType == Constants.PDF){
                            if(document.getFileType().isEmpty() || document.getFileType().equalsIgnoreCase(Constants.FILE_TYPE_PDF)){
                                pdfList.add(document);
                            }
                        }else if(docType == Constants.PHOTO){
                            if(document.getFileType().equalsIgnoreCase("image")){
                                pdfList.add(document);
                            }
                        }
                    }
                    docsAdapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(mActivity, getString(R.string.msg_add_some_data), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void initToolbar() {

        mActivity = DigitalDocsApp.getmInstance().getActivity();
        ((HomeActivity2) getActivity()).
                setUpToolbar(getString(R.string.documents), true,false);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden)initToolbar();
    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position)
                {
                    document = docsAdapter.getDocListFiltered().get(position);
                    switch (view.getId()){
                        case R.id.tvRename:
                            openRenameDialog(document);
                            break;
                        case R.id.tvDelete:
                            if (((ConnectivityManager) mActivity.getSystemService
                                    (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {

                                deleteConfirmDialog(mActivity,document);
                            }else {
                                Toast.makeText(mActivity, getString(R.string._msgno_internet_upload_document), Toast.LENGTH_SHORT).show();

                            }
                            break;

                            default:
                            if(checkPermission(mActivity)){
                                source = "update";
                                openUpdateDocActivity();
                            }

                    }
                }
            };


    @OnClick({R.id.fabAdd,})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.fabAdd:
                if(checkPermission(mActivity)){
                    if(docType == Constants.PDF){
                        startActivity(new Intent(mActivity,AddDocumentActivity.class));
                    }else{
                        startActivity(new Intent(mActivity,AddImageActivity.class));
                    }
                }


                break;
        }

    }

    private void openUpdateDocActivity() {

        Intent intent;

        if(document.getFileType().equals("image")){
            intent = new Intent(mActivity,AddImageActivity.class);
        }else{
            intent = new Intent(mActivity,AddDocumentActivity.class);
        }

        /*intent.putExtra("id",document.getId());
        intent.putExtra("name",document.getName());*/
        intent.putExtra("document",document);
        startActivity(intent);
    }


    private void deleteConfirmDialog(Context context, final Document document) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.delete_title);
        alertBuilder.setMessage(R.string.remove_doc_confirmation);
        alertBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog1, int which) {

                mDatabase.child(document.getId()).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        showHideProgress(false,frProgress);
                        dialog.dismiss();
                        Toast.makeText(mActivity, R.string.doc_removed_success, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        alertBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });


        dialog = alertBuilder.create();
        dialog.show();


    }

    private void openRenameDialog(final Document document) {

        if (((ConnectivityManager) mActivity.getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setView(R.layout.layout_doc_title);
            dialog = builder.create();
            // display dialog
            dialog.show();
            dialog.setCancelable(true);
            etTitle = dialog.findViewById(R.id.etTitle);
            tvDone = dialog.findViewById(R.id.tvDone);

            if(!document.getUpdatedName().isEmpty()) etTitle.setText(document.getUpdatedName());
            else etTitle.setText(document.getName());
            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(etTitle != null && !etTitle.getText().toString().trim().isEmpty()){
                        dialog.dismiss();
                        showHideProgress(true,frProgress);
                        document.setUpdatedName(etTitle.getText().toString().trim());
                        mDatabase.child(document.getId())
                                .setValue(document)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        showHideProgress(false,frProgress);
                                        Toast.makeText(mActivity, R.string.doc_renamed_successfully, Toast.LENGTH_SHORT).show();
                                        getDataFromServer();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showHideProgress(false,frProgress);
                                Toast.makeText(mActivity, R.string.doc_rename_error, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }else{
                        Toast.makeText(mActivity, R.string.please_enter_document_name, Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            Toast.makeText(mActivity, getString(R.string._msgno_internet_upload_document), Toast.LENGTH_SHORT).show();

        }
    }

    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showPermissionNeededDialog(context);
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void showPermissionNeededDialog(Context context) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.permission_necessary);
        alertBuilder.setMessage(R.string.external_storage_permission_needed);
        alertBuilder.setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + mActivity.getPackageName()));
                startActivity(intent);
            }
        });


        alert = alertBuilder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                try {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if(source.equals("update")){
                            openUpdateDocActivity();
                        }else{
                            startActivity(new Intent(mActivity,AddDocumentActivity.class));
                        }

                    } else {
                        showPermissionNeededDialog(mActivity);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

}
