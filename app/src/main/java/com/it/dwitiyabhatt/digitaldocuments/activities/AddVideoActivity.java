package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.it.dwitiyabhatt.digitaldocuments.BuildConfig;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.adapter.VideosAdapter;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.model.Document;
import com.it.dwitiyabhatt.digitaldocuments.util.Constants;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddVideoActivity extends BaseActivity {

    @BindView(R.id.vvPlayback)
    VideoView vvPlayback;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.fabShare)
    FloatingActionButton fabShare;

    @BindView(R.id.fabUpload)
    FloatingActionButton fabUpload;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.fabPrint)
    FloatingActionButton fabPrint;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private AlertDialog dialog;

    private EditText  etTitle;
    private TextView tvDone;
    private String imageFileName="";
    private String pushId;

    private String loggedInUserId;

    StorageReference mStorageReference;
    DatabaseReference mDatabaseReference;

    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    public final int MY_PERMISSIONS_REQUEST_VIDEO_PERMISSIONS = 321;

    private String userChoosenTask;
    private boolean isFabOpen = false;

    private  File file, tempFile;
    private Uri sourceURI;
    private Document document;
    String imageFilePath;
    private boolean isEditMode=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);
        ButterKnife.bind(this);

        //setupBannerAd();
        setVideoViewParams();
        try {
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                    Constants.MAIN_DIRECTORY );
        } catch (Exception e) {
            e.printStackTrace();
        }

        loggedInUserId = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");
        mStorageReference = FirebaseStorage.getInstance().getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(loggedInUserId).
                child(Constants.DOCUMENTS);

        if(getIntent() !=null && getIntent().getSerializableExtra("document") !=null){
            isEditMode = true;
            imageFileName = getIntent().getStringExtra("name");
            setUpToolbar(getString(R.string.update_video));
            document = (Document) getIntent().getSerializableExtra("document");
            pushId =document.getId();
            imageFileName = document.getName();
            if(DigitalDocsApp.getmInstance().getSharedPreferences().
                    getString(getString((R.string.preference_param_has_shown_print_msg)), "").isEmpty()){
                Toast.makeText(this, getString(R.string.msg_share_print_document), Toast.LENGTH_SHORT).show();
                DigitalDocsApp.getmInstance().savePreferenceDataString
                        (getString(R.string.preference_param_has_shown_print_msg),
                                "1");

            }

            checkAndLoadLocalFIle();
            showFABMenu();
        }else{
            pushId = mDatabaseReference.push().getKey();
            setUpToolbar(getString(R.string.add_video));
            document = new Document();
            document.setId(pushId);
            document.setFileType("video");
        }

     }


    private void setUpToolbar(final String title) {

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
            }
        });

    }

    private void checkAndLoadLocalFIle(){
        File mainDir = new File(Environment.getExternalStorageDirectory(),
                Constants.MAIN_DIRECTORY);
        if(mainDir.exists()){
            file  =  new File(mainDir.getAbsolutePath(), imageFileName+".mp4");
            if(file.exists()) displayFromFile(file);
            else downloadPDF();
        }else{
            downloadPDF();
        }

    }

    private void downloadPDF(){
        try {

            showHideProgress(true,frProgress);

            if (((ConnectivityManager) getSystemService
                    (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                StorageReference sRef = mStorageReference.child(Constants.STORAGE_PATH_UPLOADS + loggedInUserId
                        + "_" + imageFileName+".mp4" );
                final File localFile = File.createTempFile("resume", "mp4");

                sRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Local temp file has been created
                        tvNoData.setVisibility(View.GONE);
                        tempFile = localFile;
                        displayFromFile(localFile);
                        saveFileToMyDirectory();
                        showHideProgress(false, frProgress);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(AddVideoActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                        showHideProgress(false, frProgress);
                        tvNoData.setVisibility(View.VISIBLE);
                    }
                });
            }else{
                Toast.makeText(this, getString(R.string.msg_no_internet_download_pdf), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveFileToMyDirectory(){
        try {
            File mainDir = new File(Environment.getExternalStorageDirectory(),
                    Constants.MAIN_DIRECTORY );
            if (! mainDir.exists()){
                mainDir.mkdirs();
            }

            file  =  new File(mainDir.getAbsolutePath(), imageFileName+".mp4");

            if(file.exists()){
                file.delete();
            }

            FileInputStream inStream = new FileInputStream(tempFile.getAbsolutePath());
            FileOutputStream outStream = new FileOutputStream(file);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            inStream.close();
            outStream.close();

            document.setName(imageFileName);
            document.setCreatedDate(String.valueOf(System.currentTimeMillis()));
            mDatabaseReference.child(pushId).setValue(document).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showHideProgress(false,frProgress);


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showHideProgress(false,frProgress);
                }
            });
            //showAddWithoutCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void displayFromFile(File file) {
        /*  Glide.with(AddVideoActivity.this)
                  .load(file)
                  .skipMemoryCache(true)
                  .diskCacheStrategy(DiskCacheStrategy.NONE)
                  .into(ivImage);*/
    }

    private void startCropActivity(Uri sourceUri) {
       //TODO Add video cropper
    }

    private void displayCamera() {
        File imagesFolder = new File(Environment
                .getExternalStorageDirectory(), getResources()
                .getString(R.string.app_name));
        try {
            if (!imagesFolder.exists()) {
                boolean isCreated = imagesFolder.mkdirs();
                if (!isCreated) {
                    Toast.makeText(AddVideoActivity.this, getString(R.string.str_storage_error), Toast.LENGTH_LONG).show();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // -------- Start of v24 FileProvider concept ----------
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".mp4",         /* suffix */
                    storageDir      /* directory */
            );

            sourceURI = FileProvider.getUriForFile(AddVideoActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider", image);
            // -------- End v24 FileProvider concept ----------
            Intent intent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, sourceURI);
            //selectedFile = image.getAbsolutePath();
            try {
                startActivityForResult(intent, REQUEST_CAMERA);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void selectImage() {
        final DialogItem[] items = {
                new DialogItem(getString(R.string.record_video), R.drawable.camera2),
                new DialogItem(getString(R.string.select_video), R.drawable.gallery)
        };

        ListAdapter adapter = new ArrayAdapter<DialogItem>(
                this,
                R.layout.dialog_select_item,
                R.id.tvTitle,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(R.id.tvTitle);
                ImageView ivIcon = v.findViewById(R.id.ivIcon);
                //Put the image on the TextView
                tv.setText(items[position].text);
                ivIcon.setImageResource(items[position].icon);
                return v;
            }
        };

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.choose))
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        boolean result = checkPermission(AddVideoActivity.this);
                        if (item == 0) {
                            userChoosenTask = getString(R.string.take_photo_from_camera);
                            if (result)displayCamera();
                                //openCameraIntent();
                        } else if (item == 1) {
                            userChoosenTask = getString(R.string.choose_from_library);
                            if (result)
                                galleryIntent();
                        } else if (items[item].equals(getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                }).show();

    }

    public boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle(R.string.permission_necessary);
                    alertBuilder.setMessage(R.string.external_storage_permission_needed);
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_VIDEO_PERMISSIONS);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_VIDEO_PERMISSIONS);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_file)), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_VIDEO_PERMISSIONS:
                try {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (userChoosenTask.equals(getString(R.string.take_photo_from_camera)))
                            displayCamera();
                        else if (userChoosenTask.equals(getString(R.string.choose_from_library)))
                            galleryIntent();

                    } else {
                        //code for deny
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if (requestCode == REQUEST_CAMERA) {
                //don't compare the data to null, it will always come as  null because we are providing a file URI, so load with the imageFilePath we obtained before opening the cameraIntent
                //Glide.with(this).load(imageFilePath).into(ivImage);
                startCropActivity(sourceURI);
                // If you are using Glide.
            }else if(requestCode == UCrop.REQUEST_CROP){
                showHideProgress(true,frProgress);
                //uploadFile(UCrop.getOutput(data));
              //Glide.with(this).load(UCrop.getOutput(data)).into(ivImage);
            } else  if (requestCode == SELECT_FILE){

                    Uri uri = data.getData();
                    try{

                        vvPlayback.setVideoURI(uri);
                        MediaController mediaController = new MediaController(this);
                        mediaController.setAnchorView(vvPlayback);
                        vvPlayback.setMediaController(mediaController);
                        vvPlayback.start();
                        uploadFile(uri);
                    }catch(Exception e){
                        e.printStackTrace();
                    }

            }
        }


    }

    private void setVideoViewParams() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) vvPlayback.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        vvPlayback.setLayoutParams(params);
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }



    private void doPhotoPrint() {
        Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        PrintHelper photoPrinter = new PrintHelper(AddVideoActivity.this);
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
        photoPrinter.printBitmap("Print image", myBitmap);
    }

    @OnClick({R.id.fabAdd, R.id.fabShare, R.id.fabPrint, R.id.fabUpload})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAdd:

                if (isFabOpen) closeFABMenu();
                else showFABMenu();

                break;
            case R.id.fabShare:
                shareFile();
                break;
            case R.id.fabPrint:
                doPhotoPrint();
                break;
            case R.id.fabUpload:
                if(isEditMode){
                    selectImage();
                }else{
                    openImageialog();
                }

                break;

        }

    }

    private void openImageialog() {

        if (((ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AddVideoActivity.this);
            builder.setView(R.layout.layout_doc_title);
            dialog = builder.create();
            // display dialog
            dialog.show();
            dialog.setCancelable(true);
            etTitle = dialog.findViewById(R.id.etTitle);
            tvDone = dialog.findViewById(R.id.tvDone);

            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (etTitle != null && !etTitle.getText().toString().trim().isEmpty()) {
                        dialog.dismiss();
                        imageFileName = etTitle.getText().toString().trim();
                        selectImage();
                    } else {
                        Toast.makeText(AddVideoActivity.this, R.string.please_enter_document_name, Toast.LENGTH_SHORT).show();
                    }

                }
            });


        } else {
            Toast.makeText(AddVideoActivity.this, getString(R.string._msgno_internet_upload_document), Toast.LENGTH_SHORT).show();

        }


    }

    private void showFABMenu() {
        isFabOpen = true;
        fabPrint.animate().translationY(-getResources().getDimension(R.dimen._45sdp));
        fabShare.animate().translationY(-getResources().getDimension(R.dimen._90sdp));
        fabUpload.animate().translationY(-getResources().getDimension(R.dimen._135sdp));
    }

    private void closeFABMenu() {
        isFabOpen = false;
        fabPrint.animate().translationY(0);
        fabShare.animate().translationY(0);
        fabUpload.animate().translationY(0);
    }

    private void shareFile() {
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            //File fileWithinMyDir =  file;

            if (file != null && file.exists()) {
                intentShareFile.setType("video/mp4");
                intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Uri uri = FileProvider.getUriForFile(AddVideoActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider", file);
                intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.shating_file));
                intentShareFile.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_file_msg));

                startActivity(Intent.createChooser(intentShareFile, getString(R.string.share_file)));
            } else {
                Toast.makeText(AddVideoActivity.this, R.string.file_not_found, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadFile(Uri data) {

        StorageReference sRef = mStorageReference.child(Constants.STORAGE_PATH_UPLOADS +loggedInUserId+
                "_"+imageFileName+".mp4");
        sRef.putFile(data)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @SuppressWarnings("VisibleForTests")
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        showHideProgress(false,frProgress);
                        Toast.makeText(AddVideoActivity.this, getString(R.string.file_uploaded_success), Toast.LENGTH_SHORT).show();
                        // Upload upload = new Upload(editTextFilename.getText().toString(), taskSnapshot.getDownloadUrl().toString());
                        //mDatabaseReference.child(mDatabaseReference.push().getKey()).setValue(upload);

                        downloadPDF();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        showHideProgress(false,frProgress);
                        Toast.makeText(AddVideoActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @SuppressWarnings("VisibleForTests")
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        //textViewStatus.setText((int) progress + "% Uploading...");
                    }
                });

    }
}
