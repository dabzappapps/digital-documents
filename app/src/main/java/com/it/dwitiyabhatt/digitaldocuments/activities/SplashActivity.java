package com.it.dwitiyabhatt.digitaldocuments.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.it.dwitiyabhatt.digitaldocuments.R;
import com.it.dwitiyabhatt.digitaldocuments.application.DigitalDocsApp;
import com.it.dwitiyabhatt.digitaldocuments.baseclasses.BaseActivity;
import com.it.dwitiyabhatt.digitaldocuments.util.Utils;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        String language = DigitalDocsApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_language)), "");

        Utils.setLanguage(SplashActivity.this,language);
        setContentView(R.layout.activity_splash);
        startLoadingVideoAdd();


        new SplashTask().execute();
    }

    private class SplashTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(DigitalDocsApp.getmInstance().getSharedPreferences().
                    getString(getString((R.string.preference_param_user_language)), "").isEmpty()){
                Intent mLoginIntent = new Intent(SplashActivity.this, ChangeLanguageActivity.class);
                mLoginIntent.putExtra("source","splash");
                startActivity(mLoginIntent);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                finish();
            }
            else if(DigitalDocsApp.getmInstance().getSharedPreferences().
                    getString(getString((R.string.preference_param_user_is_logged_in)), "").isEmpty()){
                Intent mLoginIntent = new Intent(SplashActivity.this, IntroTabsActivity.class);
                startActivity(mLoginIntent);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                finish();
            }else{
                Intent mLoginIntent = new Intent(SplashActivity.this, HomeActivity2.class);
                startActivity(mLoginIntent);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                finish();

            }

        }
    }




}
